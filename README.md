# Pixabet

*How much you wanna bet it's pixabet?*

Pixabet is an open source alternative alphabet for the English language. It uses a pattern of dots, similar to braille, to represent letters, numbers, and punctuation.

Unlike braille, pixabet has not been tested as a touch-only alphabet, and it uses 8 rather than 6 cells. Pixabet is similar to a binary clock.

![Pixabet font by Klaatu](sample.png)

## Why not just use braille or some other existing alternative?

I've looked at a few alternatives, and I've found several good ones as well as some poor ones.

The simplicity of a binary clock, which I use on my desktop anyway, appeals to me, so I wanted to model my alphabet on that. While there are some existing similar attempts at this, some rely on colour, which is not acceptable for the obvious reason that it relies on colour, and others simply aren't logical (that is, they do not maintain consistency) in design.

Braille is a notable example. It's a great alphabet but since it's optimized for touch rather than by sight, many of its design choices seem almost arbitrary. I want my alphabet to resemble what we are used to, sort of a super-deformed, pixellated version of what we already know.

Some letters (c and k and q, s and z, and so on) have always bothered me, but to maintain compatibility with UNIX commands, I begrudgingly allow for them.

In short: existing solutions did not appeal to me, so I made my own.


## How can I learn Pixabet?

I've been using an alternate alphabet in hand-written notes for several years now, and Pixabet largely is a formalised version of that. I have attempted to design the alphabet to be similar to the current English alphabet, but more efficient and, sometimes, more logical. There are a lot of extra stems and flourishes in even the most basic lettering, so Pixabet does away with a lot of that.

To learn Pixabet, I suggest attacking it from two fronts:

1. Print out a glyph chart and practise writing it. This is how I developed it, and so it is how I learnt it.
1. Set Pixabet as the font for some minor element of your desktop. In the KDE Plasma desktop, it's possible to set just the font of window titles. This is a perfect candidate for Pixabet, because you don't really *need* that text, since you already know what the window is, so it's fairly simple to look at the title and read it in whatever font you choose.

Further, sit down with Pixabet once every day for about 20 minutes. Type some familiar words, look at them, study them as you type. Eventually it'll sink in. It may take a while, but it'll happen. Learning the Dvorak keyboard, for me, took probably a full month of daily practise, so stick with it.


## Design principles

Pixabet strives to be as legible as possible, and to be a "smart" alphabet. Here are its design principles:

* Lowercase and uppercase use the same glyphs (although the underlying unicode is unique, so it does not affect case-sensitivity)

* All letters touch the baseline.

* All punctuation touches the topline. Punctuation rarely descends to the baseline. This helps distinguish letters from punctuation.

* All numbers span both columns and are "inverted" representations of numbers in a vertical binary clock.

* Alphabetic glyphs are designed to resemble the letters you are already used to, only in a more efficient and structured form.


## Building

You don't need to build this. A TTF file is included in this repository. Download it and install.

If you really want to build this, open the SFD file in [fontforge](https://fontforge.github.io/en-US/) and then go to **File** > **Generate font**.


## Contributing

This font was designed and created on Linux using [fontforge](https://fontforge.github.io/en-US/).

## Bugs

This does not [yet?] have a complete glyph set.

## Issues

Report issues here, or by emailing me. I have an email address at member.fsf.org